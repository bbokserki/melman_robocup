from controller import Robot, Camera, Accelerometer
from ImageProcessing import Vision
from Moves import Move
import cv2
import time
import csv

robot = Robot()  # tworzenie klasy Robot
timestep = int(robot.getBasicTimeStep())
camera = Vision(robot.getDevice("camera"), timestep)  # dodanie kamery
accelerometer = Accelerometer("accelerometer")  # dodanie akcelerometru
move = Move(robot)  # tworzenie obiektu ruchu

#move.initialization()  # pozycja wyjsciowa ale dla starego melmana wiec przy nowych stl'ach melman spada z rowerka
time.sleep(1)

iterator = 0
sampling_time=0.01
accelerometer.enable(1)  # aktywacja akcelerometru
step_counter= {'kickLeft.csv': 259, 'kickRight.csv': 259, 'recovery_back.csv': 299, 'recovery_front.csv': 399,
    'test.csv':600,'forward/start_right_L30_T500_CoM198.csv': 422, 'forward/start_right_L40_T500_CoM198.csv': 422, 'forward/start_right_L50_T500_CoM198.csv': 422, 'forward/start_right_L60_T500_CoM198.csv': 422,
                            'forward/transfer_left_L30_T500_CoM198.csv': 262, 'forward/transfer_left_L40_T500_CoM198.csv': 262, 'forward/transfer_left_L50_T500_CoM198.csv': 262,'forward/transfer_left_L60_T500_CoM198.csv': 262,
                            'forward/transfer_right_L30_T500_CoM198.csv': 262,'forward/transfer_right_L40_T500_CoM198.csv': 262,'forward/transfer_right_L50_T500_CoM198.csv': 262,'forward/transfer_right_L60_T500_CoM198.csv': 262,
                            'forward/end_right_L30_T500_CoM198.csv': 262, 'forward/end_right_L40_T500_CoM198.csv': 262,'forward/end_right_L50_T500_CoM198.csv': 262, 'forward/end_right_L60_T500_CoM198.csv': 262,
                            'forward/end_left_L30_T500_CoM198.csv': 262, 'forward/end_left_L40_T500_CoM198.csv': 262,'forward/end_left_L50_T500_CoM198.csv': 262, 'forward/end_left_L60_T500_CoM198.csv': 262,

                            'turn_right/start_right_L5_T500_CoM198.csv': 422, 'turn_right/start_right_L10_T500_CoM198.csv': 422, 'turn_right/start_right_L12_T500_CoM198.csv': 422, 'turn_right/start_right_L15_T500_CoM198.csv': 422,
                            'turn_right/transfer_left_L5_T500_CoM198.csv': 262, 'turn_right/transfer_left_L10_T500_CoM198.csv': 262, 'turn_right/transfer_left_L12_T500_CoM198.csv': 262, 'turn_right/transfer_left_L15_T500_CoM198.csv': 262,
                            'turn_right/transfer_right_L5_T500_CoM198.csv': 262, 'turn_right/transfer_right_L10_T500_CoM198.csv': 262, 'turn_right/transfer_right_L12_T500_CoM198.csv': 262, 'turn_right/transfer_right_L15_T500_CoM198.csv': 262,
                            'turn_right/end_right_L5_T500_CoM198.csv': 262, 'turn_right/end_right_L10_T500_CoM198.csv': 262, 'turn_right/end_right_L12_T500_CoM198.csv': 262, 'turn_right/end_right_L15_T500_CoM198.csv': 262,
                            'turn_right/end_left_L5_T500_CoM198.csv': 262, 'turn_right/end_left_L10_T500_CoM198.csv': 262, 'turn_right/end_left_L12_T500_CoM198.csv': 262, 'turn_right/end_left_L15_T500_CoM198.csv': 262,

                            'turn_left/start_right_L5_T500_CoM198.csv': 422, 'turn_left/start_right_L10_T500_CoM198.csv': 422, 'turn_left/start_right_L12_T500_CoM198.csv': 422, 'turn_left/start_right_L15_T500_CoM198.csv': 422,
                            'turn_left/transfer_right_L5_T500_CoM198.csv': 262, 'turn_left/transfer_right_L10_T500_CoM198.csv': 262, 'turn_left/transfer_right_L12_T500_CoM198.csv': 263, 'turn_left/transfer_right_L15_T500_CoM198.csv': 263,
                            'turn_left/transfer_left_L5_T500_CoM198.csv': 262, 'turn_left/transfer_left_L10_T500_CoM198.csv': 262, 'turn_left/transfer_left_L12_T500_CoM198.csv': 263, 'turn_left/transfer_left_L15_T500_CoM198.csv': 263,
                            'turn_left/end_right_L5_T500_CoM198.csv': 262, 'turn_left/end_right_L10_T500_CoM198.csv': 262, 'turn_left/end_right_L12_T500_CoM198.csv': 263, 'turn_left/end_right_L15_T500_CoM198.csv': 263,
                            'turn_left/end_left_L5_T500_CoM198.csv': 262, 'turn_left/end_left_L10_T500_CoM198.csv': 262, 'turn_left/end_left_L12_T500_CoM198.csv': 263, 'turn_left/end_left_L15_T500_CoM198.csv': 263,
 }
 
move.head(0,0.2)
while robot.step(timestep) != -1:

    step = 0
    licznik = 1
    ball_direction = 'nowhere'
    move_head_direction = 'left'

    move_finished=True
    while robot.step(timestep) != -1:
    
            if move_finished == True:
                akcelerometr = accelerometer.getValues()
                ball = camera.GetBall()
                distance = ball[0]
                x_Ball = ball[1]                          # połozenie 'x' piłki. rozdzielczosc = [0,640]
                
            if move_finished == True and akcelerometr[0] > 9:
                sim_time_start = robot.getTime()
                type_of_move = 'recovery_back'
                move_finished = False
                    
            elif move_finished == True and akcelerometr[0] < -9:
                sim_time_start = robot.getTime()
                type_of_move = 'recovery_front'
                move_finished = False

            elif move_finished == True and x_Ball==0 and ball_direction=='nowhere':                             # nie widzi piłki
                sim_time_start = robot.getTime()
                type_of_move = 'move_head'
                if move_head_direction=='left':
                    head_rotation_z=100
                if move_head_direction=='right':
                    head_rotation_z=-100
                    
                head_rotation_y=0
                move_finished = False

            elif move_finished and distance<0.5:      # piłka przed robotem
                sim_time_start = robot.getTime()
                type_of_move = 'kick_left'
                move_finished = False

            elif move_finished and (x_Ball<200 or ball_direction=='left'):
                sim_time_start = robot.getTime()
                type_of_move = 'turn_left'
                num_of_rotations=2
                rotation=1
                rotation_value=10
                move_finished = False
                ball_direction='nowhere'

            elif move_finished and (x_Ball>440 or ball_direction=='right'):
                sim_time_start = robot.getTime()
                type_of_move = 'turn_right'
                num_of_rotations=2
                rotation=1
                rotation_value=10
                move_finished = False
                ball_direction='nowhere'
                
            elif move_finished and x_Ball<=440 and x_Ball>=200:
                sim_time_start = robot.getTime()
                type_of_move = 'forward'
                num_of_paces=3
                pace_num=1
                pace_length=60
                move_finished = False



            if type_of_move == 'kick_right':
                move_path='kickRight.csv'
            elif type_of_move == 'kick_left':
                move_path='kickLeft.csv'
            elif type_of_move == 'recovery_back':
                move_path='recovery_back.csv'
            elif type_of_move == 'recovery_front':
                move_path='recovery_front.csv'
                
            print(type_of_move)
            if type_of_move=='forward':
                print("wykonuje krok : " +str(pace_num) + " z : " +str(num_of_paces))
                if (pace_num==num_of_paces):
                    if (pace_num+1) % 2 == 0:
                        move_path = 'forward/end_right_L' + str(pace_length) + '_T500_CoM198.csv'
                    elif (pace_num) % 2 == 0:
                        move_path = 'forward/end_left_L' + str(pace_length) + '_T500_CoM198.csv'
                else:
                    if pace_num == 1:
                        move_path = 'forward/start_right_L' + str(pace_length) + '_T500_CoM198.csv'
                    elif (pace_num ) % 2 == 0:
                        move_path = 'forward/transfer_left_L' + str(pace_length) + '_T500_CoM198.csv'
                    elif (pace_num +1) % 2 == 0:
                        move_path = 'forward/transfer_right_L' + str(pace_length) + '_T500_CoM198.csv'


            elif type_of_move == 'turn_right':
                if (rotation == num_of_rotations):
                    if (rotation + 1) % 2 == 0:
                        move_path = 'turn_right/end_right_L' + str(rotation_value) + '_T500_CoM198.csv'
                    elif rotation % 2 == 0:
                        move_path = 'turn_right/end_left_L' + str(rotation_value) + '_T500_CoM198.csv'
                else:
                    if rotation == 1:
                            move_path = 'turn_right/start_right_L' + str(rotation_value) + '_T500_CoM198.csv'
                    elif (rotation) % 2 == 0:
                            move_path = 'turn_right/transfer_left_L' + str(rotation_value) + '_T500_CoM198.csv'
                    elif (rotation + 1) % 2 == 0:
                            move_path = 'turn_right/transfer_right_L' + str(rotation_value) + '_T500_CoM198.csv'

            elif type_of_move=='turn_left':
                if (rotation==num_of_rotations):
                    if (rotation+1) % 2  == 0:
                        move_path = 'turn_left/end_right_L' + str(rotation_value) + '_T500_CoM198.csv'
                    elif rotation % 2 == 0:
                        move_path = 'turn_left/end_left_L' + str(rotation_value) + '_T500_CoM198.csv'
                else:
                    if rotation == 1:
                        move_path = 'turn_left/start_right_L' + str(rotation_value) + '_T500_CoM198.csv'
                    elif (rotation) % 2 == 0:
                        move_path = 'turn_left/transfer_left_L' + str(rotation_value) + '_T500_CoM198.csv'
                    elif (rotation +1) % 2 == 0:
                        move_path = 'turn_left/transfer_right_L' + str(rotation_value) + '_T500_CoM198.csv'

            sim_time = robot.getTime()
            step = int((sim_time - sim_time_start) / sampling_time)# zamiana czasu symulacji na numer kroku

            if type_of_move=='forward':
                if step == step_counter[move_path] and pace_num == num_of_paces:  # jeśli przeszło przez liczbę kroków, to zatrzymaj pętlę
                    move_finished=True
                elif step >= step_counter[move_path]:
                    sim_time_start = robot.getTime()
                    pace_num+=1

            if type_of_move=='turn_right' or type_of_move == 'turn_left':
                if step == step_counter[move_path] and rotation == num_of_rotations:  # jeśli przeszło przez liczbę kroków, to zatrzymaj pętlę
                    move_finished=True
                elif step == step_counter[move_path]:
                    sim_time_start = robot.getTime()
                    rotation += 1

            if type_of_move == 'kick_left' or type_of_move == 'kick_right' or type_of_move == 'recovery_back' or type_of_move =='recovery_front':
                if step >= step_counter[move_path]:  # jeśli przeszło przez liczbę kroków, to zatrzymaj pętlę
                    move_finished=True

            if type_of_move == 'move_head':
                if step%20==0:
                    ball = camera.GetBall()
                    x_Ball = ball[1]
                    if x_Ball !=0:
                        if move_head_direction=='left':
                            ball_direction='left'
                        if move_head_direction=='right':
                            ball_direction='right'
                    
                if step <100:
                    move.head(3.14*step*head_rotation_z/10000,0.2)
                elif step>99 and step<200:
                    move.head(3.14 * head_rotation_z*(200-step) / 10000,0.2)
                elif step >= 200: ## daje >200 bo cos przeskakuje czasem kroki i jak przeskoczy 200 to kiepsko
                    if move_head_direction=='left':
                        move_head_direction='right'
                    elif move_head_direction=='right':
                        move_head_direction='left'
                    move_finished = True
            else:
                if step == 0: step = 1  # zabezpieczenie (nie ma zerowego kroku
                move.perform_move(move_path,step)  # sciezka do ruchu oraz numer kroku ktory ma byc wykonany z tego pliku


            print("krok: ", step, ", czas ruchu: ", round(sim_time - sim_time_start, 3))
            print("odczyt z akcelerometru: ", accelerometer.getValues())



accelerometer.disable()  # dezaktywacja akcelerometru

